#!/bin/bash

TERMINAL_WIDTH=$(tput cols)
TERMINAL_HEIGHT=$(tput lines)
MINIMUM_WIDTH=30
MINIMUM_HEIGHT=25

declare -A GAME_BOARD
COLUMN_COUNT=20
ROW_COUNT=20

X_OFFSET=10
Y_OFFSET=5

DELTA_X=0
DELTA_Y=0

HEAD_X=$(($COLUMN_COUNT / 2))
HEAD_Y=$(($ROW_COUNT / 2))
SNAKE=($HEAD_X $HEAD_Y)

FOOD_AVAILABLE=0
FOOD_X=12
FOOD_Y=4
FOOD_TIME_TO_RESET=10
FOOD_TIME_REMAINING=5

SCORE=0
SCORE_PER_FRUIT=100

DT=0.2
KEY='s'

for ((ROW=0;ROW<ROW_COUNT;ROW++)) do
    for ((COL=0;COL<COLUMN_COUNT;COL++)) do
        GAME_BOARD[$ROW,$COL]="."
    done
done

function check_terminal() {
	if [[ $TERMINAL_HEIGHT -lt $TERMINAL_HEIGHT || $TERMINAL_WIDTH -lt $MINIMUM_WIDTH ]]; then
		echo "Minimum terminal size is $TERMINAL_HEIGHT x $MINIMUM_WIDTH"
		exit 1;
	fi
}

function print_char() {
	echo -en "\e[$2;${1}f$3";
}

function print_board() {
	clear
	
	for ((COL=0;COL<COLUMN_COUNT + 2;COL++)) do
		print_char $(($X_OFFSET + $COL * 2 - 2)) $(($Y_OFFSET - 1)) "^^"
	done
	
	for ((ROW=0;ROW<ROW_COUNT;ROW++)) do
		print_char $(($X_OFFSET - 2)) $(($ROW + $Y_OFFSET)) "<|"
		for ((COL=0;COL<COLUMN_COUNT;COL++)) do
			print_char $(($X_OFFSET + $COL * 2)) $(($ROW + $Y_OFFSET)) "${GAME_BOARD[$ROW,$COL]} "
		done
		print_char $(($X_OFFSET + $COLUMN_COUNT * 2)) $(($ROW + $Y_OFFSET)) "|>"
	done
	
	for ((COL=0;COL<COLUMN_COUNT + 2;COL++)) do
		print_char $(($X_OFFSET + $COL * 2 - 2)) $(($ROW_COUNT + $Y_OFFSET)) "vv"
	done
}

function print_snake() {
	for ((S=0;S<${#SNAKE[@]};S=S+2)) do
		print_char $(($X_OFFSET + 2 * (${SNAKE[$S]}))) $(($Y_OFFSET+${SNAKE[$(($S+1))]})) "\u001b[41m  \u1b[0m"
	done
}

function check_is_part_of_snake() {
	local X=$1
	local Y=$2
		
	for ((S=0;S<$((${#SNAKE[@]}-2));S=S+2)) do
		local CURRENT_X=${SNAKE[$S]}
		local CURRENT_Y=${SNAKE[$(($S+1))]}
		
		print_char 0 $(($Y_OFFSET + 30)) $CURRENT_X
		print_char 0 $(($Y_OFFSET + 30)) $CURRENT_Y
	
		if [[ $X -eq $CURRENT_X && $Y -eq $CURRENT_Y ]]; then
			return 0;
		fi
	done
		
	return 1;
}

function print_food() {
	if [[ $FOOD_AVAILABLE -eq 1 ]]; then
		print_char $(($X_OFFSET + 2 * $FOOD_X)) $(($Y_OFFSET + $FOOD_Y)) "\u001b[42m()\u1b[0m"
	fi
}

function grow_snake() {
	local HEAD_X=${SNAKE[-2]}
	local HEAD_Y=${SNAKE[-1]}
	SNAKE=(${SNAKE[@]} $HEAD_X $HEAD_Y)
}

function check_collisions() {
	local HEAD_X=${SNAKE[-2]}
	local HEAD_Y=${SNAKE[-1]}
	
	# Check collisions with walls
	if [[ $HEAD_X -lt 0 || $HEAD_X -ge $COLUMN_COUNT || $HEAD_Y -lt 0 || $HEAD_Y -ge $ROW_COUNT ]]; then
		print_game_over
		exit 1
	fi
	
	# Check collisions with snake itself
	check_is_part_of_snake $HEAD_X $HEAD_Y
	if [[ $? -eq 0 ]]; then
		print_game_over
		exit 1
	fi
	
	#Check collisions with food
	if [[ $HEAD_X -eq $FOOD_X && $HEAD_Y -eq $FOOD_Y ]]; then
		FOOD_AVAILABLE=0
		FOOD_TIME_REMAINING=$FOOD_TIME_TO_RESET
		SCORE=$(($SCORE + $SCORE_PER_FRUIT))
		grow_snake
		echo -en "\007"
	fi
}

function handle_user_input() {
	case "$KEY" in
		'w') 
			DELTA_Y=-1
			DELTA_X=0
		;;
		's') 
			DELTA_Y=1
			DELTA_X=0
		;;
		'a') 
			DELTA_Y=0
			DELTA_X=-1
		;;
		'd') 
			DELTA_Y=0
			DELTA_X=1
		;;
		'q') 
			exit 1
		;;
	esac
}

function update_snake_position() {
	local PREVIOUS_HEAD_X=${SNAKE[-2]}
	local PREVIOUS_HEAD_Y=${SNAKE[-1]}
	
	SNAKE=(${SNAKE[@]:2} $(($PREVIOUS_HEAD_X + $DELTA_X)) $(($PREVIOUS_HEAD_Y + $DELTA_Y)))
}

function generate_food() {
	if [[ $FOOD_AVAILABLE -eq 0 ]] && [[ $FOOD_TIME_REMAINING -gt 0 ]]; then
		FOOD_TIME_REMAINING=$(($FOOD_TIME_REMAINING - 1))
		return
	fi
	
	if [[ $FOOD_AVAILABLE -eq 1 ]]; then
		return
	fi

	local NEW_FOOD_X=-1
	local NEW_FOOD_Y=-1
	
	while : ; do
		NEW_FOOD_X=$(($RANDOM % $ROW_COUNT))
		NEW_FOOD_Y=$(($RANDOM % $COLUMN_COUNT))
	
		check_is_part_of_snake $NEW_FOOD_X $NEW_FOOD_Y
		[[ $? -eq 0 ]] || break
	done
	
	
	FOOD_X=$NEW_FOOD_X
	FOOD_Y=$NEW_FOOD_Y
	FOOD_AVAILABLE=1
}

function print_score() {
	print_char $X_OFFSET $(($Y_OFFSET - 2)) "Score: $SCORE"
}
  
function print_splash() {
	echo '           /^\/^\           _____  _   _   ___   _   __ _____'
	echo '         _| o| O |         /  ___|| \ | | / _ \ | | / /|  ___|'
	echo '\/     /~     \_/ \        \ `--. |  \| |/ /_\ \| |/ / | |__'
	echo ' \____|__________/  \       `--. \| . ` ||  _  ||    \ |  __|'
	echo '        \_______      \    /\__/ /| |\  || | | || |\  \| |___'
	echo '                `\     \   \____/ \_| \_/\_| |_/\_| \_/\____/'
	echo '                  |     |                  \'
	echo '                 /      /                    \'
	echo '                /     /                       \\'
	echo '              /      /                         \ \'
	echo '             /     /                            \  \'
	echo '           /     /             _----_            \   \'
	echo '          /     /           _-~      ~-_         |   |'
	echo '         (      (        _-~    _--_    ~-_     _/   |'
	echo '          \      ~-____-~    _-~    ~-_    ~-_-~    /'
	echo '            ~-_           _-~          ~-_       _-~'
	echo '               ~--______-~                ~-___-~'

	sleep 2
}

function print_game_over() {
	print_char 0 $(($Y_OFFSET + $(($COLUMN_COUNT / 2)) - 5)) ' _________________________________________________________'
	print_char 0 $(($Y_OFFSET + $(($COLUMN_COUNT / 2)) - 4)) '|                                                         |'
	print_char 0 $(($Y_OFFSET + $(($COLUMN_COUNT / 2)) - 3)) '|   _____                         ____                 _  |'
	print_char 0 $(($Y_OFFSET + $(($COLUMN_COUNT / 2)) - 2)) '|  / ____|                       / __ \               | | |'
	print_char 0 $(($Y_OFFSET + $(($COLUMN_COUNT / 2)) - 1)) '| | |  __  __ _ _ __ ___   ___  | |  | |_   _____ _ __| | |'
	print_char 0 $(($Y_OFFSET + $(($COLUMN_COUNT / 2)) - 0)) '| | | |_ |/ _` |  _ ` _ \ / _ \ | |  | \ \ / / _ \  __| | |'
	print_char 0 $(($Y_OFFSET + $(($COLUMN_COUNT / 2)) + 1)) '| | |__| | (_| | | | | | |  __/ | |__| |\ V /  __/ |  |_| |'
	print_char 0 $(($Y_OFFSET + $(($COLUMN_COUNT / 2)) + 2)) '|  \_____|\__,_|_| |_| |_|\___|  \____/  \_/ \___|_|  (_) |'
	print_char 0 $(($Y_OFFSET + $(($COLUMN_COUNT / 2)) + 3)) '|                                                         |'
	print_char 0 $(($Y_OFFSET + $(($COLUMN_COUNT / 2)) + 4)) '|_________________________________________________________|'
	
	sleep 2
	clear
}

function update() {
	handle_user_input
	generate_food
	check_collisions
	update_snake_position

	print_board
	print_food
	print_snake
	print_score

	( sleep $DT; kill -14 $$ )&
}

check_terminal

clear
print_splash

trap update 14
update

while :; do
    read -rsn1 KEY
done
